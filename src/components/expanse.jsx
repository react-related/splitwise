import React, { Component } from "react";

export default class Friends extends Component {

  render() {

    const { expanses } = this.props;
    return (
      <div className="friends">
      {
        Object.keys(expanses).map((key, index)=>{
          return(<div className="friend u_pad5 u_marT10" key={key}>{expanses[key].text}</div>)
        })
      }
      </div>
    );
  }
}
