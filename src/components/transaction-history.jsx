import React, { Component } from "react";

export default class TransactionHistory extends Component {

  getPayers(payers){
    let payerList = [];
    payers.map((obj, index) =>{
      payerList.push(obj.label);
    });
    return payerList.join(', ');
  }

  render() {
    const { description, amount, payee, payers, id } = this.props.transaction;
    let payerText = this.getPayers(payers);

    return (
      <div className="transaction">
        <div className="u_pad5">Description: {description}</div>
        <div className="u_pad5">Amount: {amount}</div>
        <div className="u_pad5">Payee: {payee.label}</div>
        <div className="u_pad5">Payers: {payerText}</div>
      </div>
    );
  }
}
