import React, { Component, Children } from 'react';

export default class ModalPanel extends Component{

  render(){
    const props = this.props;
    const children = this.props.children;
    return (
      <div className="modal">
        <div className="modal-content">
          <div className="modal-close" onClick={props.closeModal}>&#10006;</div>
          <div className="modal-header">{props.title}</div>
          {Children.map(children, child => React.cloneElement(child, { ...props}))}
        </div>
      </div>
    );
  }
}
