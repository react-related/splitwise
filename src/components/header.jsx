import React, { Component } from 'react';
import { Link } from 'react-router-dom';

export default class Header extends Component{

  openModal(modalName){
    this.props.openModal(modalName);
  }

  toggle(pageName){
    this.props.toggle(pageName);
  }

  render(){
    return (
      <div className="nav-item u_pad20 u_padL50">
        <Link  to="/" className="u_dspInlnBlk u_width20" onClick={this.toggle.bind(this,'transactions')}>
          Transaction History
        </Link>
        <Link to="friends" className="u_dspInlnBlk u_width20" onClick={this.toggle.bind(this, 'friends')}>
          Friends
        </Link>
        <div className="u_dspInlnBlk u_width20 u_crsrpntr" onClick={this.openModal.bind(this,'friend')}>
          Add Friend
        </div>
        <div className="u_dspInlnBlk u_width20 u_crsrpntr" onClick={this.openModal.bind(this,'transaction')}>
          Add Transaction
        </div>
      </div>
    )
  }
}
