import React, { Component } from 'react';

export default class AddFriend extends Component{

  constructor(props){
    super(props);
    this.state={
      friendName: ''
    }
    this.enterFriendName = this.enterFriendName.bind(this);
    this.addFriend = this.addFriend.bind(this);
    this.closeModal = this.closeModal.bind(this);
  }

  enterFriendName(e){
    this.setState({friendName: e.target.value});
  }

  addFriend(){
    if(this.state.friendName){
      this.props.addFriend(this.state.friendName);
    }
    this.setState({friendName: ''});
  }

  closeModal(){
    if(this.state.friendName){
      this.props.addFriend(this.state.friendName);
    }
    this.props.closeModal();
  }

  render(){
    return (
      <div className="section">
        <div className="input-field">
          <label className="u_mar10">Friend's Name</label>
          <input className="u_mar10" type='text' value={this.state.friendName}
            onChange={this.enterFriendName}
          />
        </div>
        <div className="button-submit">
          <button onClick={this.addFriend}>Add Another</button>
          <button onClick={this.closeModal}>Done</button>
        </div>
      </div>
    );
  }
}
