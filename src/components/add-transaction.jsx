import React, { Component } from 'react';
import ReactSelect from 'react-select';
import { memoiz } from '../utils';

export default class AddTransaction extends Component{

  constructor(props){
    super(props);
    this.state = {
      transactionName: '',
      amount: '',

    };
    this.addTransaction = this.addTransaction.bind(this);
    this.enterTransactionName = this.enterTransactionName.bind(this);
    this.enterAmount = this.enterAmount.bind(this);
    this.friendList = [];
    this.selectedPayee = '';
    this.selectedPayers = [];
  }

  componentWillMount(){
    this.props.friendList.map((friend, index) => {
      this.friendList.push({label: friend, value: index})
    })
  }

  enterTransactionName(e){
    this.setState({
      transactionName: e.target.value
    });
  }

  enterAmount(e){
    this.setState({
      amount: e.target.value
    });
  }

  selectPeople = memoiz((people, red) => (e) =>{
    if(people == 'Payee'){
      this.selectedPayee = e;
    }else if(people == 'Payers') {
      this.selectedPayers = e;
    }
  })

  addTransaction(){
    this.props.addTransaction({
      description: this.state.transactionName,
      amount: this.state.amount,
      payee: this.selectedPayee,
      payers: this.selectedPayers
    });
  }

  render(){

    return(
      <div className="section">
        <div className="input-field">
          <label className="u_mar10">Transaction's Name</label>
          <input className="u_mar10" type='text' value={this.state.transactionName}
            onChange={this.enterTransactionName}
          />
        </div>
        <div className="input-field">
          <label className="u_mar10">Amount</label>
          <input className="u_mar10" type='number' value={this.state.amount}
            onChange={this.enterAmount}
          />
        </div>
        <div className="select-payee u_marT20">
          <label className="u_mar10">Select Payee</label>
          <ReactSelect
            className="u_width50 u_dspInlnBlk"
            name="Payee"
            onChange={this.selectPeople('Payee', {a: 2})}
            options={this.friendList}
          />
        </div>
        <div className="select-payee u_marT20">
          <label className="u_mar10">Select Payers</label>
          <ReactSelect
            className="u_width75 u_dspInlnBlk"
            name="Payers"
            onChange={this.selectPeople('Payers', {a: 3})}
            options={this.friendList}
            isMulti
          />
        </div>
        <div className="button-submit">
          <button onClick={this.addTransaction}>Done</button>
        </div>
      </div>
    )
  }
}
