import dispatcher from "../dispatcher";

export function addFriendAction(name){
  dispatcher.dispatch({
    type: "Add_FRIEND",
    name
  });
}

export function createTransactionAction(transaction) {
  dispatcher.dispatch({
    type: "CREATE_TRANSACTION",
    transaction
  });
}
