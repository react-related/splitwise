import React from "react";
import ReactDOM from "react-dom";
import { Route, BrowserRouter, Switch } from "react-router-dom";
import Home from "./containers/home";
import '../public/stylesheets/main.scss';

const app = document.getElementById('app');

ReactDOM.render(
  <BrowserRouter>
    <Switch>
      <Route path="/" component={Home}></Route>
    </Switch>
  </BrowserRouter>,
app);
