import React, { Component } from "react";
import Header from '../components/header';
import ModalPanel from '../components/modal-panel';
import AddFriend from '../components/add-friend';
import AddTransaction from '../components/add-transaction';
import TransactionHistory from '../components/transaction-history';
import Expanse from '../components/expanse';
import TransactionStore from '../stores/transaction-store';
import FriendStore from '../stores/friend-store';
import { addFriendAction, createTransactionAction } from '../actions/splitaction';

export default class Home extends Component {

  constructor(){
    super()
    this.state = {
      friendModal: false,
      transactionModal: false,
      showExpanses: false,
      showTransactions: true,
      transactionList: TransactionStore.getAllTransactions(),
      expanses: TransactionStore.getAllExpanses(),
      friendList: FriendStore.getAllFriends()
    }

    this.addTransaction = this.addTransaction.bind(this);
    this.addFriend = this.addFriend.bind(this);
    this.getStoreValues = this.getStoreValues.bind(this)
    TransactionStore.on("change", this.getStoreValues);
  }

  componentWillUnmount(){
    TransactionStore.removeListener("change", this.getStoreValues);
  }

  getStoreValues() {
    this.setState({
      transactionList: TransactionStore.getAllTransactions(),
      expanses: TransactionStore.getAllExpanses(),
      friendList: FriendStore.getAllFriends()
    });
  }

  openModal(modalName){
    if(modalName === "friend"){
      this.setState({friendModal: true, transactionModal: false});
    }else if(modalName === "transaction"){
      this.setState({friendModal: false, transactionModal: true});
    }
  }

  closeModal(modalName){
    if(modalName === "friend"){
      this.setState({friendModal: false, transactionModal: false});
    }else if(modalName === "transaction"){
      this.setState({friendModal: false, transactionModal: false});
    }
  }

  addFriend(name){
    addFriendAction(name);
  }

  addTransaction(transaction){
    createTransactionAction(transaction);
    this.closeModal('transaction');
  }

  toggle(page){
    if(page == 'transactions'){
      this.setState({showExpanses: false, showTransactions: true})
    }else if (page == 'friends') {
      this.setState({showExpanses: true, showTransactions: false});
    }
  }

  render() {

    const { showExpanses, showTransactions, transactionList, friendList, expanses } = this.state;

    return (
      <div className="home">
        <Header
          openModal={this.openModal.bind(this)}
          toggle={this.toggle.bind(this)}
        />
        {
          showTransactions
          ? transactionList.map((transaction, index) =>{
              return(
                <div key={transaction.id} className="trans">
                  <TransactionHistory transaction={transaction}/>
                </div>
              )
            })
          : ''
        }
        {
          showExpanses
          ? <Expanse expanses={expanses}/>
          : ''
        }
        {
          this.state.friendModal
          ? <ModalPanel title="Add Friend"
              closeModal={this.closeModal.bind(this, 'friend')}
              addFriend={this.addFriend}
              >
              <AddFriend />
            </ModalPanel>
          : ''
        }
        {
          this.state.transactionModal
          ? <ModalPanel title="Add Transaction"
              closeModal={this.closeModal.bind(this, 'transaction')}
              addTransaction={this.addTransaction}
              friendList={friendList}
              >
              <AddTransaction />
            </ModalPanel>
          : ''
        }
      </div>
    );
  }
}
