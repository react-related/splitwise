export function memoiz(funct) {
  let cache = {};
  return function(){
    let aArgs = Array.prototype.slice.call(arguments);
    let sArgs = JSON.stringify(aArgs);
    if(cache[sArgs]){
      return cache[sArgs]
    }else {
      return (cache[sArgs] = funct.apply(this, aArgs));
    }
  }
}
