import { EventEmitter } from "events";
import dispatcher from "../dispatcher";

class FriendStore extends EventEmitter {

  constructor(){
    super()
    this.friends = JSON.parse(localStorage.getItem('friends')) || [];
  }

  addFriendStore(name){
    this.friends.push(name);
    localStorage.setItem('friends', JSON.stringify(this.friends));
    this.emit('change');
  }

  getAllFriends(){
    return this.friends;
  }

  handleActions(action){
    switch (action.type) {
      case 'Add_FRIEND':
        this.addFriendStore(action.name);
        break;
    }
  }
}

const friendStore = new FriendStore;
dispatcher.register(friendStore.handleActions.bind(friendStore));
export default friendStore;
