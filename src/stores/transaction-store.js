import { EventEmitter } from "events";
import dispatcher from "../dispatcher";

class TransactionStore extends EventEmitter {

  constructor(){
    super()
    this.transactions = JSON.parse(localStorage.getItem('transactions')) || [];
    this.expanses = JSON.parse(localStorage.getItem('expanses')) || {};
  }

  idGenerator(){
    return Math.random().toString(36).substr(2, 4);
  }

  getAllExpanses(){
    return this.expanses;
  }

  getAmount(prevAmount, amount, operation){
    if(operation === 'plus'){
      return parseFloat((prevAmount + amount).toFixed(2));
    }else if(operation === 'negative'){
      return parseFloat((prevAmount - amount).toFixed(2));
    }else {
      return parseFloat((amount - prevAmount).toFixed(2));
    }
  }

  createExpanses(payeeValue, payerValue, payeeName, payerName, amount){
    if(payeeValue == payerValue){
      return;
    }
    amount = parseFloat(amount);
    let key = payeeValue+'_'+payerValue;
    let reverseKey = payerValue+'_'+payeeValue;
    if(this.expanses[key]){
      this.expanses[key]["amount"] = this.getAmount(this.expanses[key]["amount"],  amount, 'plus');
      this.expanses[key]["text"] = payerName + ' owes ' + this.expanses[key]["amount"] + ' to '+ payeeName;
    }else if(this.expanses[reverseKey]){
      if(this.expanses[reverseKey]["amount"] < amount){
        this.expanses[key] = {};
        this.expanses[key]["amount"] = this.getAmount(this.expanses[reverseKey]["amount"],  amount, '');
        this.expanses[key]["text"] = payerName + ' owes ' + this.expanses[key]["amount"] + ' to '+ payeeName;
        delete this.expanses[reverseKey];
      }else {
        this.expanses[reverseKey]["amount"] = this.getAmount(this.expanses[reverseKey]["amount"],  amount, 'negative');
        this.expanses[reverseKey]["text"] = payeeName + ' owes ' + this.expanses[reverseKey]["amount"] + ' to '+ payerName;
      }
    }else {
      this.expanses[key] = {
        text: payerName + ' owes ' + amount + ' to '+ payeeName,
        amount: amount
      };

    }
  }

  createTransaction(transaction){
    transaction.id = this.idGenerator();

    let payeeValue = transaction.payee.value;
    let payeeName = transaction.payee.label;
    let amount = parseFloat(transaction.amount) / (transaction.payers.length + 1);
    transaction.payers.map((obj, index) =>{
      if(obj.value == payeeValue){
        amount = parseFloat(transaction.amount) / transaction.payers.length;
        transaction.payers.splice(index, 1);
      }
    });

    transaction.payers.map((obj, index) =>{
      this.createExpanses(payeeValue, obj.value, payeeName, obj.label, amount.toFixed(2));
    });
    localStorage.setItem('expanses', JSON.stringify(this.expanses));
    this.transactions.push(transaction);
    localStorage.setItem('transactions', JSON.stringify(this.transactions));
    this.emit('change');
  }


  getAllTransactions(){
    return this.transactions;
  }

  handleActions(action){
    switch (action.type) {
      case 'CREATE_TRANSACTION':
        this.createTransaction(action.transaction);
        break;
      default:
    }
  }
}

const transactionStore = new TransactionStore;
dispatcher.register(transactionStore.handleActions.bind(transactionStore));

export default transactionStore;
